﻿using System.IO.Compression;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Runtime.InteropServices.ComTypes;

namespace HourSteamInstaller {
    internal class Installer {
        internal static bool done = false;
        //This could always be better adjusted to take more input instead of just being set.
        //Then outside of your class but in your namespace
        [ComImport]
        [Guid("00021401-0000-0000-C000-000000000046")]
        internal class ShellLink {
        }

        [ComImport]
        [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        [Guid("000214F9-0000-0000-C000-000000000046")]
        internal interface IShellLink {
            void GetPath([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszFile, int cchMaxPath, out IntPtr pfd, int fFlags);
            void GetIDList(out IntPtr ppidl);
            void SetIDList(IntPtr pidl);
            void GetDescription([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszName, int cchMaxName);
            void SetDescription([MarshalAs(UnmanagedType.LPWStr)] string pszName);
            void GetWorkingDirectory([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszDir, int cchMaxPath);
            void SetWorkingDirectory([MarshalAs(UnmanagedType.LPWStr)] string pszDir);
            void GetArguments([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszArgs, int cchMaxPath);
            void SetArguments([MarshalAs(UnmanagedType.LPWStr)] string pszArgs);
            void GetHotkey(out short pwHotkey);
            void SetHotkey(short wHotkey);
            void GetShowCmd(out int piShowCmd);
            void SetShowCmd(int iShowCmd);
            void GetIconLocation([Out, MarshalAs(UnmanagedType.LPWStr)] StringBuilder pszIconPath, int cchIconPath, out int piIcon);
            void SetIconLocation([MarshalAs(UnmanagedType.LPWStr)] string pszIconPath, int iIcon);
            void SetRelativePath([MarshalAs(UnmanagedType.LPWStr)] string pszPathRel, int dwReserved);
            void Resolve(IntPtr hwnd, int fFlags);
            void SetPath([MarshalAs(UnmanagedType.LPWStr)] string pszFile);
        }

        internal static string runnerLua = "repeat task.wait() until game:IsLoaded()\r\nif game.PlaceId ~= 5732973455 then\r\n    return\r\nend\r\nrepeat task.wait() until getrenv()._G.ScriptDatabase ~= nil\r\n\r\nprint(\"searching for multiplayer install...\")\r\nlocal allDirectories = listfiles(\"\")\r\nlocal localBuild = nil\r\nfor _,folder in allDirectories do\r\n    if isfolder(folder) == false then\r\n        continue\r\n    end\r\n    for _,file in listfiles(folder) do\r\n        local splitted = string.split(file,\"\\\\\")\r\n        file = splitted[#splitted]\r\n        if file ~= \"hoursMultiplayer.txt\" then\r\n            continue\r\n        end\r\n        localBuild = folder\r\n    end\r\nend\r\nif localBuild == nil then\r\n    warn(\"can't find hours multiplayer, did you forget to install it?\")\r\n    return\r\nend\r\nlocal data = readfile(`{localBuild}/Client/boot.lua`)\r\nloadstring(data)()(localBuild)";
        internal static void Install() {
            string path = Directory.GetCurrentDirectory();
            string[] directoriesToExist = new string[] { "workspace", "autoexec" };
            foreach (string d in directoriesToExist) {
                if (!Directory.Exists(Path.Combine(path,d))) {
                    Console.WriteLine($"{d} does not exist, are you sure this is an executor's folder?");
                    return;
                }
            }

            Console.WriteLine("Creating autorunning launch script...");

            string autoexec = Path.Combine(path, "autoexec");
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(autoexec, "hoursMultiplayer.lua"))) {
                outputFile.Write(runnerLua);
            }

            Console.WriteLine("Downloading HourSteam source code...");

            string pathToDownload = $"{path}/workspace/HourSteam.zip";
            using (var client = new WebClient()) {
                client.DownloadFile("https://gitlab.com/nptnc/hoursteam/-/archive/main/hoursteam-main.zip", pathToDownload);
            }

            Console.WriteLine("Extracting source code...");

            ZipFile.ExtractToDirectory(pathToDownload, $"{path}/workspace",true);
            File.Delete(pathToDownload);

            Console.WriteLine("Creating shortcut...");

            IShellLink link = (IShellLink)new ShellLink();

            // setup shortcut information
            link.SetDescription("Launch HourSteam server.");
            link.SetPath($"{path}/workspace/hoursteam-main/Proxy/build/Proxy.exe");

            // save it
            IPersistFile file = (IPersistFile)link;
            file.Save(Path.Combine(path, "Server.lnk"), false);

            Console.WriteLine("Done!");
            done = true;
        }
    }
}
